★ [[../Tool-Support.org][← Back to Tool Support]] ★ [[../Overview.org][↑ Overview page]] ★

* Orgdown support for Plain Org

- Platform: iOS
- License: non-free
- Price: $8.99
- [[https://plainorg.com][plainorg.com]]
- [[https://reddit.com/r/plainorg][reddit]]
- [[https://twitter.com/plainorg][twitter]]
- [[https://apps.apple.com/app/id1578965002][App Store]]
- last Orgdown1 syntax assessment: 2021-12-12 using version 1.1.0 by [[https://xenodium.com][Alvaro Ramirez]]

| *Percentage of Orgdown1 syntax support:* | 92 |
#+TBLFM: @1$2=(remote(toolsupport,@>$2)/86)*100;%.0f

** Details

- legend:
  - *1* → works as simple text
  - *2* → at least one of:
    - syntax highlighting
    - support for inserting/collapse/expand/…
    - support for toggling (e.g., checkboxes, states, …)

#+NAME: toolsupport
| Element                                                                     | OD1 | OD1 comment              |
|-----------------------------------------------------------------------------+-----+--------------------------|
| syntax elements do not need to be separated via empty lines                 |   2 |                          |
|-----------------------------------------------------------------------------+-----+--------------------------|
| *bold*                                                                        |   2 |                          |
| /italic/                                                                      |   2 |                          |
| _underline_                                                                   |   2 |                          |
| +strike through+                                                              |   2 |                          |
| =code=                                                                        |   2 |                          |
| ~commands~                                                                    |   2 |                          |
| Combinations like *bold and /italic/ text*                                      |   2 |                          |
| : example                                                                   |   2 |                          |
| [[https://example.com][link *bold* test]]                                                              |   2 |                          |
| [[https://example.com][link /italic/ test]]                                                            |   2 |                          |
| [[https://example.com][link +strike through+ test]]                                                    |   2 |                          |
| [[https://example.com][link =code= test]]                                                              |   2 |                          |
| [[https://example.com][link ~commands~ test]]                                                          |   2 |                          |
| ≥5 dashes = horizontal bar                                                  |   1 |                          |
|-----------------------------------------------------------------------------+-----+--------------------------|
| Headings using asterisks                                                    |   2 |                          |
|-----------------------------------------------------------------------------+-----+--------------------------|
| Unordered list item with "-"                                                |   2 |                          |
| Ordered list item with 1., 2., …                                            |   2 |                          |
| Checkbox + unordered list item with "-"                                     |   2 |                          |
| Checkbox + ordered list item with 1., 2., …                                 |   2 |                          |
| Mixed lists of ordered and unordered items                                  |   2 |                          |
| Multi-line list items with collapse/expand                                  |   1 |                          |
| Multi-line list items with support for (auto-)indentation                   |   1 |                          |
|-----------------------------------------------------------------------------+-----+--------------------------|
| Example block                                                               |   2 |                          |
| Quote block                                                                 |   2 |                          |
| Verse block                                                                 |   1 |                          |
| Src block                                                                   |   2 |                          |
|-----------------------------------------------------------------------------+-----+--------------------------|
| Comment lines with "# <foobar>"                                             |   1 |                          |
| Comment block                                                               |   2 |                          |
|-----------------------------------------------------------------------------+-----+--------------------------|
| https://gitlab.com/publicvoit/orgdown plain URL without brackets            |   2 |                          |
| [[https://gitlab.com/publicvoit/orgdown]] URL with brackets without description |   2 |                          |
| [[https://gitlab.com/publicvoit/orgdown][OD homepage]] URL with brackets with description                              |   2 |                          |
|-----------------------------------------------------------------------------+-----+--------------------------|
| Basic table functionality                                                   |   2 |                          |
| Table cells with *bold*                                                       |   2 |                          |
| Table cells with /italic/                                                     |   2 |                          |
| Table cells with _underline_                                                  |   2 |                          |
| Table cells with +strike through+                                             |   2 |                          |
| Table cells with =code=                                                       |   2 |                          |
| Table cells with ~commands~                                                   |   2 |                          |
| Auto left-aligning of text                                                  |   1 |                          |
| Auto right-aligning of numbers like "42.23"                                 |   1 |                          |
|-----------------------------------------------------------------------------+-----+--------------------------|
| Other syntax elements are interpreted at least as normal text (or better)   |   2 | Keywords, Tags, Priority |
| Other syntax elements: linebreaks respected (or better)                     |   2 | Fill paragraph           |
|-----------------------------------------------------------------------------+-----+--------------------------|
| *Sum*                                                                         |  79 |                          |
#+TBLFM: @>$2=vsum(@I$2..@>>$2)
